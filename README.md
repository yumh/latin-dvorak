# Latin-Dvorak

Fork of [N0rthw1nd/Latin-Dvorak](https://github.com/N0rthw1nd/Latin-Dvorak) keyboard layout with better vim-experience on tty.

## Main Features

This are additional features over the original [N0rthw1nd/Latin-Dvorak](https://github.com/N0rthw1nd/Latin-Dvorak)

 * `esc` and `caps lock` swapped on tty

## OS Compatibility

This layout is mantained **only** for **Parabola GNU/Linux-libre**, but *should* work seamlessy in every OS that use Xorg. The `tty` mapping instead may work only on *GNU/Linux*.

I'm not planning to make it compatible with other linux distros, but if some of you are interested in it, I accept any form of collaboration. Maybe I'll port this to FreeBSD, but I'm not totally sure.

## Installation

**Arch Linux and derivates**
```
$ git clone https://github.com/omar-polo/latin-dvorak
$ cd latin-dvorak
$ makepkg -si
```

**ProTip**: set `KEYMAP=dvp-it` in `/etc/vconsole.conf` (or `keymap="dvp-it"` in `/etc/conf.d/keymaps` if you use openrc) to auto-set this layout for the tty.

## Usage

In xorg:
```
$ setxkbmap dvp-it
```

In tty:
```
$ sudo loadkeys dvp-it
```

New layout loaded... Enjoy!

# KeyBoard Preview:

![Latin Dvorak Preview](https://gitlab.com/yumh/latin-dvorak/raw/master/Latin-Dovrak-layout.png "Latin-Dvorak Preview")
